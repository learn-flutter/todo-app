import 'package:flutter/material.dart';
import 'package:todo_app/plan_provider.dart';
import 'package:todo_app/views/PlanCreatorScreen.dart';

void main() {
  runApp(PlanProvider(child: TodoApp()));
}

class TodoApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return PlanProvider(
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.purple,
        ),
        //home: PlanProvider(child: PlanScreen(),),
        home: PlanCreatorScreen()
      ),
    );
  }
}

