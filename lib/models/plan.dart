import 'package:todo_app/models/task.dart';

class Plan{
  String name = '';
  List<Task> tasks = [];


  int get completeCount => tasks
    .where((task) => task.complete)
    .length;

  String get completenessMessage =>
      '$completeCount completas de ${tasks.length} tareas';
}